trigger SelespersonTrigger on Salesperson__c (before insert) {
    
    switch on Trigger.operationType {
        when BEFORE_INSERT {
            // Asignar un carro por default al que este 
            // relacionadao el vendedor
            QueryCars.assignSellerToCarByDefault( Trigger.new );
        }
        when else {
            System.debug( 'Not case' );
        }
    }
}