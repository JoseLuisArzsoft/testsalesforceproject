public with sharing class QueryCars {
    public static void assignSellerToCarByDefault( List<Salesperson__c> listSellers) {
        Car__c firstCar = [SELECT Id FROM Car__c WHERE Id='a02Hp00001Yfk8KIAR'];
        for( Salesperson__c seller: listSellers ){
            seller.Car__c = firstCar.Id;
        }
    }
}