public with sharing class TestCollections {
    public TestCollections() {
        // this.debugListAndMap();
        this.debugQuery();

    }

    public Void debugQuery(){
        // Account oneAccount = [SELECT Name FROM Account WHERE Id='001Hp00002bOzq0IAC' LIMIT 1];
        
        Set<Contact> listContact = [SELECT Id, AccountId FROM Contact WHERE AccountId != NULL];
        
        Map<String, Account> idContactsUnionAccount = new Map<String, Account>();

        for( Contact currentContact: listContact) {
            // Account currentAccount = [SELECT Name FROM Account WHERE Id= :currentContact.AccountId LIMIT 1];
            idContactsUnionAccount.put( currentContact.Id, currentAccount );
            
            System.debug( '->> Account Id\'s'+currentContact );
        }
    }

    public Void debugListAndMap(){
        List<String> strNames = new List<String>{
            'Poncho',
            'Irving',
            'Rafael',
            'José'
        };

        Map<String, Integer> mapAges = new Map<String, Integer>();

        for( String strName: strNames){
            mapAges.put( strName,  Integer.valueof( (Math.random()*90) ));
        }

        System.debug( '====> My Map Ages: '+mapAges );

    }
}