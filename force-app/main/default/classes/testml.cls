// Crear un ojeto de Opportunity, 
// Account y Solicitud de Aprovación

public with sharing class testml {
    
    public static String test() {
        Solicitud_de_aprovaci_n__c objSolicitud   = new Solicitud_de_aprovaci_n__c(
            Name = 'Validación de Contrato Trabajador',
            Tipo_de_aprovaci_n__c   = 'Primario',
            Fecha_de_solicitud__c   = date.newinstance(2023, 2, 17),
            Fecha_de_vencimiento__c = date.newinstance(2023, 2, 17),
            Estatus__c              = 'Detalles'
        );

        Opportunity objOpportunity = new Opportunity(
            Name      = 'Venta de muebles',
            StageName = 'Propecting',
            CloseDate = date.newinstance(2023, 2, 17)
        );

        Account objAccount     = new Account(
            Name = 'Bancomer'
        );
        
        objAccount.Ownership = 'Public';
        String result = objSolicitud.Name + '>>' + objOpportunity.Name + '>>' +objAccount.Name;
        
        return result;
    }
}