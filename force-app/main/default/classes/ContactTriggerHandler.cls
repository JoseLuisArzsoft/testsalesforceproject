public with sharing class ContactTriggerHandler {
    public static void validationFieldsSuccefullByInsert( List<Contact> listContact ) {
        Integer score;

        for( Contact currentContact: listContact ) {
            score = 0;
            if( currentContact.Title != null ){
                currentContact.It_Has_Title__c = true;
                score++;
            } else { currentContact.It_Has_Title__c = false; }
            if( currentContact.AccountId != null){
                currentContact.Completed_field_Account__c = true;
                score++;
            }else { currentContact.Completed_field_Account__c = false; }

            if( currentContact.Phone != null ){
                currentContact.Completed_field_Phone__c = true;
                score++;
            }else {currentContact.Completed_field_Phone__c = false;}

            if( currentContact.Languages__c=='English' || currentContact.Languages__c=='english' ){
                currentContact.It_Has_English__c=true;
                score++;
            }else{currentContact.It_Has_English__c=false;}

            currentContact.Score_Contact_3__c = score;

        }
    }

    public static void validationFieldsSuccefullByUpdate( List<Contact> lsNewContact, Map<Id, Contact> mpOldContact ) {
        Contact currentOldContact;
        for( Contact currentNewContact: lsNewContact ){
            currentOldContact = mpOldContact.get( currentNewContact.Id );
            currentNewContact.Score_Contact_3__c += updateScoreContact3By( 
                currentOldContact.Title, 
                currentNewContact.Title,
                currentNewContact.It_Has_Title__c
            );
            currentNewContact.Score_Contact_3__c += updateScoreContact3By( 
                currentOldContact.AccountId, 
                currentNewContact.AccountId,
                currentNewContact.Completed_field_Account__c
            );

            currentNewContact.Score_Contact_3__c += updateScoreContact3By( 
                currentOldContact.Phone, 
                currentNewContact.Phone,
                currentNewContact.Completed_field_Phone__c
            );

            System.debug('****** Value New Language: '+currentNewContact.Languages__c);
            System.debug('****** Value Old Language: '+currentOldContact.Languages__c);
            if( currentOldContact.Languages__c != currentNewContact.Languages__c ){
                if( currentNewContact.Languages__c=='English' ) { //textArea
                    // currentNewContact.It_Has_English__c = !currentNewContact.It_Has_English__c;
                    currentNewContact.Score_Contact_3__c= currentNewContact.Score_Contact_3__c+1;
                }else{ 
                    // currentNewContact.It_Has_English__c = !currentNewContact.It_Has_English__c;
                    currentNewContact.Score_Contact_3__c = currentNewContact.Score_Contact_3__c-1;
                }
            }
        }
    }

    private static Integer updateScoreContact3By( String oldField, String newField, Boolean newFieldStatus ){
        if( oldField != newField ){
            return  getResultBetweenOldAndNew( oldField, newField, newFieldStatus );
        }else{
            return 0;
        }
    }

    private static Integer getResultBetweenOldAndNew( String oldValue, String newValue, Boolean stageNewValue ) {
        if( newValue != null && oldValue == null ) {
            stageNewValue = !stageNewValue;
            return 1;
        }else if ( oldValue!=null && newValue!=null ){
            return 0;
        }else if ( newValue == null ) {
            stageNewValue = !stageNewValue;
            return -1;
        }else{ return 0;}
    }
}