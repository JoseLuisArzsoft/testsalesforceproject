public with sharing class QueryLessons {
    public static void upsertQuery() {
        Contact newContact = new Contact(
            FirstName='Peter',
            LastName ='Parker',
            Title    ='Student'
            );
        
        Contact oldContact = [SELECT Id, Name FROM Contact WHERE Id='003Hp00002rgcB7IAI'];
        oldContact.FirstName = 'Poncho Ocho';
            
        List<Contact> listUpsertContacts = new List<Contact>{
            newContact,
            oldContact
        };

        upsert listUpsertContacts;
        
        
        List<String> listFirstNames = new List<String>();
        for( Contact contact: listUpsertContacts ){
            listFirstNames.add( contact.FirstName );
        }

        List<Contact> listGetContacts = [SELECT Id, Name FROM Contact WHERE FirstName=:listFirstNames];


        System.debug('>>>> Contactos Actualizados y creados: '+listGetContacts);
    }
}