public with sharing class TestListsAndSets {
    public TestListsAndSets() {
        List<String>   testNames   = new List<String>();
        List<Product2> listProducts= new List<Product2>();
        Set<String>    idUsers     = new Set<String>();
        
        
        for( integer item=1; item<=4; item++ ){
            testNames.add( 'Test-'+item );
            idUsers.add( 'User-'+item );
            listProducts.add( new Product2(Name='Product-'+item) );
        }

        System.debug( '=> Test Names: '   +testNames    );
        System.debug( '=> List Products: '+listProducts );
        System.debug( '=> Id Users: '     +idUsers      );
    }

}