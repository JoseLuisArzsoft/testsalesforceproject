public with sharing class QueryOpportunity {
    public static void updateStageToPropecting(){
        List<Opportunity> listOpp = [SELECT Name, StageName FROM Opportunity WHERE StageName='Prospecting' ];
        for( Opportunity currentOpp: listOpp ){
            if( currentOpp.Name == 'New Opportunity' ){
                currentOpp.StageName = 'Closed Won';
            }
        }

        update listOpp;

    }
}