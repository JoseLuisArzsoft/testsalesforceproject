/* 

1. Crear 5 registros de Contact con al menos 4 campos llenos
 aparate del requerido (LastName) <Se realiza la colección con una list>
2. Hacer un insert a esos registros <Al finalizar de coleccionar los 
contactos utiliza 'insert' para insertar los contactos creados> 
3. actualizar esos registros agregandole un numero al nombre, ('test' - 'test 1') 
4. Eliminar dos objetos
5. Hacer un mapa de los contactos de llave es el contact 
    id, y de valor es el objcontact 
    

Prepara el siguiente proceso:
TestQuery objQuery = new TestQuery();
objQuery.createFiveRegisters();
objQuery.updateContacts();
objQuery.removeTwoContacts();
Map<String, Contact> mapContacts = objQuery.createMapContacts();
*/

public with sharing class TestQuery {
    public List<Contact> listContact;

    public TestQuery() {
        this.listContact = new List<Contact>();
        System.debug('Lista creada');
    }

    public Map<String, Contact> createMapContacts(){
        Map<String, Contact> mapContacts = new Map<String, Contact>();
        for(Contact contact: this.listContact) {
            mapContacts.put( contact.Id, contact );
        }

        System.debug('>>>>Map de Contactos: '+mapContacts);
        return mapContacts;
    }

    public void removeTwoContacts(){
        if( this.listContact.size() != 0 ){
            List<Contact> listDeleteContacts = new List<Contact>();
            listDeleteContacts.add( this.listContact.get(0) );
            listDeleteContacts.add( this.listContact.get(1) );

            delete listDeleteContacts;
            System.debug('>>>>Contactos eliminados: '+listDeleteContacts);
        }
    }

    public void updateContacts() {
        if( this.listContact.size() != 0 ){
            for( Integer item=0; item < this.listContact.size(); item++ ){
                this.listContact[item].FirstName += ' '+(item+1);
            }

            update this.listContact;
            System.debug('>>>>Contactos Actualizados: '+this.listContact);
        }
    }

    public void createFiveRegisters() {
        if( this.listContact.size() == 0 ){
            for(Integer item=0; item<5; item++ ){
                this.listContact.add(
                    new Contact(
                        FirstName='Poncho',
                        LastName ='Torres',
                        Title    ='Doctor',
                        Phone    ='2337591165',
                        Birthdate =date.newinstance(2003, 11, 17)
                    )
                );
            }
    
            insert this.listContact;
            System.debug('>>>>Contactos creados: '+this.listContact);
        }
    }
}