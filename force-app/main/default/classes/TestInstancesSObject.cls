public with sharing class TestInstancesSObject {
    public static String createInstances() {
        Contact    objContact; 
        Offer__c   objOffer;
        Product2   objProduct;
        QuickText  objText;
        Payment    objPayment;


        Opportunity objOpportunity;
        Account     objAccount;
        Solicitud_de_aprovaci_n__c objSolicitud;


        String strResult = '';


        // Nuevas instancias
        objContact = new Contact(
            FirstName = 'Pedro',
            LastName  = 'Torres'
        );
        strResult ='Contact Name: '+objContact.FirstName+'>>>';

        objOffer   = new Offer__c( 
            Target_Close_Date__c=date.newinstance(2023, 2, 17)
        );
        
        strResult +='Result Date: '+objOffer.Target_Close_Date__c+'>>>';

        objProduct = new Product2(
            Name = 'Bananas'
        );
        strResult += 'Product Name: '+objProduct.Name+'>>>';

        objText = new QuickText(
            Name   ='Bienvenidad de la empresa',
            Message='Bienvenido a nuestra org.'
        );
        strResult += 'QuickText Name: '+objText.Name+'>>>';

        objPayment = new Payment(
            Amount=3000,
            Status='Draft',
            Type='Sale',
            ProcessingMode='Salesforce'
        );
        strResult += 'Payment Amount: '+objPayment.Amount+'>>>';

        // Viejas instancias

        objSolicitud   = new Solicitud_de_aprovaci_n__c(
            Name = 'Validación de Contrato Trabajador',
            Tipo_de_aprovaci_n__c   = 'Primario',
            Fecha_de_solicitud__c   = date.newinstance(2023, 2, 17),
            Fecha_de_vencimiento__c = date.newinstance(2023, 2, 17),
            Estatus__c              = 'Detalles'
        );

        objOpportunity = new Opportunity(
            Name      = 'Venta de muebles',
            StageName = 'Propecting',
            CloseDate = date.newinstance(2023, 2, 17)
        );

        objAccount     = new Account( Name = 'Bancomer' );
        
        objAccount.Ownership = 'Public';



        strResult += objSolicitud.Name + '>>' + objOpportunity.Name + '>>' +objAccount.Name;
        
        return strResult;
    }
}